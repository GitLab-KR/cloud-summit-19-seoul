# 구글 클라우드 서밋 서울! GitLab Event!

##  `Event #1 :  방명록을 남겨주시면 연락드립니다. 그리고 GitLab 티셔츠를 선물로 드려요.`

### 방명록 작성하기

* GitLab 계정이 없으시다면, [GitLab 가입](https://gitlab.com/users/sign_in) 합니다.
* GitLab 계정이 있으시다면 [방명록 이슈작성](https://gitlab.com/GitLab-KR/cloud-summit-19-seoul/issues/new) 을 통해 Title, Description을 이용해서 방명록을 남겨 주세요.
  * `Title` 에는 `환영 멘트` 
  * `Description` 에는 `하고 싶은 이야기` 를 적어주세요.
* 추첨을 통해 **GitLab 티셔츠**를 등록하신 GitLab 사용자 아이디를 통해서 연락을 드립니다.

### 방명록 살펴보기 😆 -> https://gitlab-kr.gitlab.io/cloud-summit-19-seoul/


