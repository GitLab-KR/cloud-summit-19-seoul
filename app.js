const request = require("request")

new Promise((resolve, reject) => {
    var request_url = "https://gitlab.com/api/v4/projects/15186386/issues?private_token="+ process.env.TOKEN+"&per_page=200" ;
    request.get(
        {url: request_url },
        (err, response, body) => {
        const bodyJson = JSON.parse(body)

        resolve(bodyJson)
        }
    )
}).then(value => {
    var fs = require('fs'), ejs = require("ejs");
    let users = {value: value};

    function ejs2html(path, value) {
        fs.readFile(path, 'utf8', function (err, data) {
            if (err) { console.log(err); return false; }
            var ejs_string = data,
                template = ejs.compile(ejs_string),
                html = template(value);
            fs.writeFile(path + '.html', html, function(err) {
                if(err) { console.log(err); return false }
                return true;
            });
        });
    }
    ejs2html(__dirname+"/index.ejs", users)
})

